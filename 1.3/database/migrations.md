# Database: Migrations

## Introduction

Migrations are like version control for your database, allowing your team to easily modify and share the application's database schema.

## Generating Migrations

To create a migration, use the `make:migration`:

```bash
php modulus make:migration users
```

The new migration will be placed in your `storage/migrations` directory.

The `add_to_{table}` option may be used to indicate the name of the table:

```bash
php modulus make:migration dateofbirth add_to_users
```

## Migration Structure

A migration class contains two methods: `up` and `down`. The `up` method is used to add new tables, columns, or indexes to your database, while the `down` method should reverse the operations performed by the `up` method.

```php
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;

class UsersMigration
{
    /**
      * Run the migrations.
      *
      * @return void
      */
    public function up()
    {
        Capsule::schema()->create("users", function ($table) {
            $table->increments("id");
            $table->string("username");
            $table->string("email");
            $table->string("password");
            $table->timestamps();
        });
    }

    /**
      * Reverse the migrations.
      *
      * @return void
      */
    public function down()
    {
        Capsule::schema()->dropIfExists("users");
    }
}
```

## Running Migrations

To run all of your outstanding migrations, execute the `migrate` Modulus command:

```bash
php modulus migrate
```

### Running a single Migration

To run 1 migrarion, execute the `migrate` Modulus command with the name of the migration:

```bash
php modulus migrate 2018_05_12_16_32_users
```

### Dropping a Migration

To drop a migration, you can use the down or drop keyword:

```bash
php modulus migrate 2018_05_12_16_32_users down
```

```bash
php modulus migrate 2018_05_12_16_32_users drop
```

### Dropping all migrations

Dropping all migrations is as easy as using `all down` or `all drop`:

```bash
php modulus migrate all down
```

```bash
php modulus migrate all drop
```

## Tables

### Creating Tables

To create a new database table, use the `create` method. The `create` method accepts two arguments. The first is the name of the table, while the second is a `Closure` which receives a `Blueprint` object that may be used to define the new table:

```php
Capsule::schema()->create('users', function (Blueprint $table) {
    $table->increments('id');
});
```

Of course, when creating the table, you may use any of the schema builder's [column methods](#creating-columns) to define the table's columns.

#### Checking For Table / Column Existence

You may easily check for the existence of a table or column using the `hasTable` and `hasColumn` methods:

```php
if (Capsule::schema()->hasTable('users')) {
    //
}
```

```php
if (Capsule::schema()->hasColumn('users', 'email')) {
    //
}
```

### Renaming / Dropping Tables

To rename an existing database table, use the `rename` method:

```php
Capsule::schema()->rename($from, $to);
```

To drop an existing table, you may use the `drop` or `dropIfExists` methods:

```php
Capsule::schema()->drop('users');
```

```php
Capsule::schema()->dropIfExists('users');
```

#### Renaming Tables With Foreign Keys

Before renaming a table, you should verify that any foreign key constraints on the table have an explicit name in your migration.

<a name="columns"></a>
## Columns

<a name="creating-columns"></a>
### Creating Columns

The `table` method on the `Schema` facade may be used to update existing tables. Like the `create` method, the `table` method accepts two arguments: the name of the table and a `Closure` that receives a `Blueprint` instance you may use to add columns to the table:

```php
Capsule::schema()->table('users', function (Blueprint $table) {
    $table->string('email');
});
```

#### Available Column Types

Of course, the schema builder contains a variety of column types that you may specify when building your tables:

Command  |  Description
-------  |  -----------
`$table->bigIncrements('id');`  |  Auto-incrementing UNSIGNED BIGINT (primary key) equivalent column.
`$table->bigInteger('votes');`  |  BIGINT equivalent column.
`$table->binary('data');`  |  BLOB equivalent column.
`$table->boolean('confirmed');`  |  BOOLEAN equivalent column.
`$table->char('name', 100);`  |  CHAR equivalent column with an optional length.
`$table->date('created_at');`  |  DATE equivalent column.
`$table->dateTime('created_at');`  |  DATETIME equivalent column.
`$table->dateTimeTz('created_at');`  |  DATETIME (with timezone) equivalent column.
`$table->decimal('amount', 8, 2);`  |  DECIMAL equivalent column with a precision (total digits) and scale (decimal digits).
`$table->double('amount', 8, 2);`  |  DOUBLE equivalent column with a precision (total digits) and scale (decimal digits).
`$table->enum('level', ['easy', 'hard']);`  |  ENUM equivalent column.
`$table->float('amount', 8, 2);`  |  FLOAT equivalent column with a precision (total digits) and scale (decimal digits).
`$table->geometry('positions');`  |  GEOMETRY equivalent column.
`$table->geometryCollection('positions');`  |  GEOMETRYCOLLECTION equivalent column.
`$table->increments('id');`  |  Auto-incrementing UNSIGNED INTEGER (primary key) equivalent column.
`$table->integer('votes');`  |  INTEGER equivalent column.
`$table->ipAddress('visitor');`  |  IP address equivalent column.
`$table->json('options');`  |  JSON equivalent column.
`$table->jsonb('options');`  |  JSONB equivalent column.
`$table->lineString('positions');`  |  LINESTRING equivalent column.
`$table->longText('description');`  |  LONGTEXT equivalent column.
`$table->macAddress('device');`  |  MAC address equivalent column.
`$table->mediumIncrements('id');`  |  Auto-incrementing UNSIGNED MEDIUMINT (primary key) equivalent column.
`$table->mediumInteger('votes');`  |  MEDIUMINT equivalent column.
`$table->mediumText('description');`  |  MEDIUMTEXT equivalent column.
`$table->morphs('taggable');`  |  Adds `taggable_id` UNSIGNED INTEGER and `taggable_type` VARCHAR equivalent columns.
`$table->multiLineString('positions');`  |  MULTILINESTRING equivalent column.
`$table->multiPoint('positions');`  |  MULTIPOINT equivalent column.
`$table->multiPolygon('positions');`  |  MULTIPOLYGON equivalent column.
`$table->nullableMorphs('taggable');`  |  Adds nullable versions of `morphs()` columns.
`$table->nullableTimestamps();`  |  Alias of `timestamps()` method.
`$table->point('position');`  |  POINT equivalent column.
`$table->polygon('positions');`  |  POLYGON equivalent column.
`$table->rememberToken();`  |  Adds a nullable `remember_token` VARCHAR(100) equivalent column.
`$table->smallIncrements('id');`  |  Auto-incrementing UNSIGNED SMALLINT (primary key) equivalent column.
`$table->smallInteger('votes');`  |  SMALLINT equivalent column.
`$table->softDeletes();`  |  Adds a nullable `deleted_at` TIMESTAMP equivalent column for soft deletes.
`$table->softDeletesTz();`  |  Adds a nullable `deleted_at` TIMESTAMP (with timezone) equivalent column for soft deletes.
`$table->string('name', 100);`  |  VARCHAR equivalent column with a optional length.
`$table->text('description');`  |  TEXT equivalent column.
`$table->time('sunrise');`  |  TIME equivalent column.
`$table->timeTz('sunrise');`  |  TIME (with timezone) equivalent column.
`$table->timestamp('added_on');`  |  TIMESTAMP equivalent column.
`$table->timestampTz('added_on');`  |  TIMESTAMP (with timezone) equivalent column.
`$table->timestamps();`  |  Adds nullable `created_at` and `updated_at` TIMESTAMP equivalent columns.
`$table->timestampsTz();`  |  Adds nullable `created_at` and `updated_at` TIMESTAMP (with timezone) equivalent columns.
`$table->tinyIncrements('id');`  |  Auto-incrementing UNSIGNED TINYINT (primary key) equivalent column.
`$table->tinyInteger('votes');`  |  TINYINT equivalent column.
`$table->unsignedBigInteger('votes');`  |  UNSIGNED BIGINT equivalent column.
`$table->unsignedDecimal('amount', 8, 2);`  |  UNSIGNED DECIMAL equivalent column with a precision (total digits) and scale (decimal digits).
`$table->unsignedInteger('votes');`  |  UNSIGNED INTEGER equivalent column.
`$table->unsignedMediumInteger('votes');`  |  UNSIGNED MEDIUMINT equivalent column.
`$table->unsignedSmallInteger('votes');`  |  UNSIGNED SMALLINT equivalent column.
`$table->unsignedTinyInteger('votes');`  |  UNSIGNED TINYINT equivalent column.
`$table->uuid('id');`  |  UUID equivalent column.
`$table->year('birth_year');`  |  YEAR equivalent column.

<a name="column-modifiers"></a>
### Column Modifiers

In addition to the column types listed above, there are several column "modifiers" you may use while adding a column to a database table. For example, to make the column "nullable", you may use the `nullable` method:

```php
Capsule::schema()->table('users', function (Blueprint $table) {
    $table->string('email')->nullable();
});
```

Below is a list of all the available column modifiers. This list does not include the [index modifiers](#creating-indexes):

Modifier  |  Description
--------  |  -----------
`->after('column')`  |  Place the column "after" another column (MySQL)
`->autoIncrement()`  |  Set INTEGER columns as auto-increment (primary key)
`->charset('utf8')`  |  Specify a character set for the column (MySQL)
`->collation('utf8_unicode_ci')`  |  Specify a collation for the column (MySQL/SQL Server)
`->comment('my comment')`  |  Add a comment to a column (MySQL)
`->default($value)`  |  Specify a "default" value for the column
`->first()`  |  Place the column "first" in the table (MySQL)
`->nullable($value = true)`  |  Allows (by default) NULL values to be inserted into the column
`->storedAs($expression)`  |  Create a stored generated column (MySQL)
`->unsigned()`  |  Set INTEGER columns as UNSIGNED (MySQL)
`->useCurrent()`  |  Set TIMESTAMP columns to use CURRENT_TIMESTAMP as default value
`->virtualAs($expression)`  |  Create a virtual generated column (MySQL)

<a name="modifying-columns"></a>
### Modifying Columns

#### Prerequisites

Before modifying a column, be sure to add the `doctrine/dbal` dependency to your `composer.json` file. The Doctrine DBAL library is used to determine the current state of the column and create the SQL queries needed to make the specified adjustments to the column:

```bash
composer require doctrine/dbal
```

#### Updating Column Attributes

The `change` method allows you to modify some existing column types to a new type or modify the column's attributes. For example, you may wish to increase the size of a string column. To see the `change` method in action, let's increase the size of the `name` column from 25 to 50:

```php
Capsule::schema()->table('users', function (Blueprint $table) {
    $table->string('name', 50)->change();
});
```

We could also modify a column to be nullable:

```php
Capsule::schema()->table('users', function (Blueprint $table) {
    $table->string('name', 50)->nullable()->change();
});
```

> {note} Only the following column types can be "changed": bigInteger, binary, boolean, date, dateTime, dateTimeTz, decimal, integer, json, longText, mediumText, smallInteger, string, text, time, unsignedBigInteger, unsignedInteger and unsignedSmallInteger.

#### Renaming Columns

To rename a column, you may use the `renameColumn` method on the Schema builder. Before renaming a column, be sure to add the `doctrine/dbal` dependency to your `composer.json` file:

```php
Capsule::schema()->table('users', function (Blueprint $table) {
    $table->renameColumn('from', 'to');
});
```

> {note} Renaming any column in a table that also has a column of type `enum` is not currently supported.

<a name="dropping-columns"></a>
### Dropping Columns

To drop a column, use the `dropColumn` method on the Schema builder. Before dropping columns from a SQLite database, you will need to add the `doctrine/dbal` dependency to your `composer.json` file and run the `composer update` command in your terminal to install the library:

```php
Capsule::schema()->table('users', function (Blueprint $table) {
    $table->dropColumn('votes');
});
```

You may drop multiple columns from a table by passing an array of column names to the `dropColumn` method:

```php
Capsule::schema()->table('users', function (Blueprint $table) {
    $table->dropColumn(['votes', 'avatar', 'location']);
});
```

> {note} Dropping or modifying multiple columns within a single migration while using a SQLite database is not supported.

#### Available Command Aliases

Each index method accepts an optional second argument to specify the name of the index. If omitted, the name will be derived from the names of the table and column(s).

Command  |  Description
-------  |  -----------
`$table->dropRememberToken();`  |  Drop the `remember_token` column.
`$table->dropSoftDeletes();`  |  Drop the `deleted_at` column.
`$table->dropSoftDeletesTz();`  |  Alias of `dropSoftDeletes()` method.
`$table->dropTimestamps();`  |  Drop the `created_at` and `updated_at` columns.
`$table->dropTimestampsTz();` |  Alias of `dropTimestamps()` method.

<a name="indexes"></a>
## Indexes

<a name="creating-indexes"></a>
### Creating Indexes

The schema builder supports several types of indexes. First, let's look at an example that specifies a column's values should be unique. To create the index, we can chain the `unique` method onto the column definition:

```php
$table->string('email')->unique();
```

Alternatively, you may create the index after defining the column. For example:

```php
$table->unique('email');
```

You may even pass an array of columns to an index method to create a compound (or composite) index:

```php
$table->index(['account_id', 'created_at']);
```

modulusPHP will automatically generate a reasonable index name, but you may pass a second argument to the method to specify the name yourself:

```php
$table->unique('email', 'unique_email');
```

#### Available Index Types

Command  |  Description
-------  |  -----------
`$table->primary('id');`  |  Adds a primary key.
`$table->primary(['id', 'parent_id']);`  |  Adds composite keys.
`$table->unique('email');`  |  Adds a unique index.
`$table->index('state');`  |  Adds a plain index.
`$table->spatialIndex('location');`  |  Adds a spatial index. (except SQLite)

<a name="dropping-indexes"></a>
### Dropping Indexes

To drop an index, you must specify the index's name. By default, modulusPHP automatically assigns a reasonable name to the indexes. Concatenate the table name, the name of the indexed column, and the index type. Here are some examples:

Command  |  Description
-------  |  -----------
`$table->dropPrimary('users_id_primary');`  |  Drop a primary key from the "users" table.
`$table->dropUnique('users_email_unique');`  |  Drop a unique index from the "users" table.
`$table->dropIndex('geo_state_index');`  |  Drop a basic index from the "geo" table.
`$table->dropSpatialIndex('geo_location_spatialindex');`  |  Drop a spatial index from the "geo" table  (except SQLite).

If you pass an array of columns into a method that drops indexes, the conventional index name will be generated based on the table name, columns and key type:

```php
Capsule::schema()->table('geo', function (Blueprint $table) {
    $table->dropIndex(['state']); // Drops index 'geo_state_index'
});
```

<a name="foreign-key-constraints"></a>
### Foreign Key Constraints

modulusPHP also provides support for creating foreign key constraints, which are used to force referential integrity at the database level. For example, let's define a `user_id` column on the `posts` table that references the `id` column on a `users` table:

```php
Capsule::schema()->table('posts', function (Blueprint $table) {
    $table->unsignedInteger('user_id');

    $table->foreign('user_id')->references('id')->on('users');
});
```

You may also specify the desired action for the "on delete" and "on update" properties of the constraint:

```php
$table->foreign('user_id')
      ->references('id')->on('users')
      ->onDelete('cascade');
```

To drop a foreign key, you may use the `dropForeign` method. Foreign key constraints use the same naming convention as indexes. So, we will concatenate the table name and the columns in the constraint then suffix the name with "_foreign":

```php
$table->dropForeign('posts_user_id_foreign');
```

Or, you may pass an array value which will automatically use the conventional constraint name when dropping:

```php
$table->dropForeign(['user_id']);
```

You may enable or disable foreign key constraints within your migrations by using the following methods:

```php
Capsule::schema()->enableForeignKeyConstraints();
```

```php
Capsule::schema()->disableForeignKeyConstraints();
```
