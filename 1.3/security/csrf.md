# CSRF Protection

## Introduction

modulusPHP makes it easy to protect your application from cross-site request forgery (CSRF) attacks. Cross-site request forgeries are a type of malicious exploit whereby unauthorized commands are performed on behalf of an authenticated user.

modulusPHP automatically generates a CSRF "token" for each active user session managed by the application. This token is used to verify that the authenticated user is the one actually making the requests to the application.

Anytime you define a HTML form in your application, you should include a hidden CSRF token field in the form so that the request cam be validated. You may use the {% csrf %} Modulus Touch short-hand to generate the token field:

```html
<form method="POST" action="/profile">
    {% csrf %}
    ...
</form>
```

Or:

```
<form method="POST" action="/profile">
    <input type="hidden" name="csrf_token" value="{{ $csrf_token }}">
    ...
</form>
```

modulusPHP automatically verify's all url's passed through the `web.php` routes file. Url's passed through the `api.php` routes file do not require csrf protection. You can however, roll out your own validation feature for all `api` requests.

### CSRF Tokens & JavaScript

When building JavaScript driven applications, it is convenient to have your JavaScript HTTP library automatically attach the CSRF token to every outgoing request. By default, the `resources/assets/js/bootstrap.js` file registers the value of the csrf-token meta tag with the Axios HTTP library. If you are not using this library, you will need to manually configure this behavior for your application.

## X-CSRF-TOKEN

In addition to checking for the CSRF token as a POST parameter, the csrf request validator will also check for the X-CSRF-TOKEN request header. You could, for example, store the token in a HTML meta tag:

```html
<meta name="csrf-token" content="{{ $csrf_token }}">
```

Then, once you have created the meta tag, you can instruct a library like jQuery to automatically add the token to all request headers. This provides simple, convenient CSRF protection for your AJAX based applications:

```javascript
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
```