# Mail

<a name="introduction"></a>
## Introduction

The Modulus Email component uses PHPMailer to allow you to easily send emails. Here's how you can get started.

The first thing you need to do, is make sure you have configured the Email component in the `.env` file.
```env
# Mail
MAIL_USERNAME=example@domain.com
MAIL_PASSWORD=secret
MAIL_FROM_NAME=foo
MAIL_HOST=smtp.domain.com
MAIL_PORT=465 # or 587
MAIL_SMTP_SECURE=ssl # or tls
```

Here's an example of Gmail's configurations:
```env
# Mail
MAIL_USERNAME=foo@gmail.com
MAIL_PASSWORD=secret
MAIL_FROM_NAME=foo
MAIL_HOST=smtp.gmail.com
MAIL_PORT=465
MAIL_SMTP_SECURE=ssl
```

Let's look at the code.

```php
<?php

namespace App\Http\Controllers;

use ModulusPHP\Mail\Mail;
use App\Http\Controllers\Controller;

class EmailController extends Controller
{
  public function send(Request $request)
  {
    if ($request->hasInput('email')) {
      $recipient = $request->data('email');

      $email = new Mail;
      $email->to($recipient);
      $email->view('app.email.welcome', compact('recipient'));

      $response = $email->send('Welcome to modulusPHP.', 'Welcome');
      if ($response['status'] == 'success') {
        //
      }
    }
  }
}
```

You can attach a Modulus Touch view and pass variables to your email, this means you can easily create UI's for your emails. The `send` method returns a response which can be used to see if an email was successfully sent.

The response will look something like this:
```php
Array
(
    [status] => success
)
```

## send
To send an email, you need to use the `send` method. You can pass a body and a subject.

### Parameters

Argument  |  Type  |  Description
--------  |  ----  |  -----------
body      |  String  |  The email body (message).
subject   |  String  |  The subject of the email.

Basic usage:
```php
$email = new Mail;
$email->to('donald@foo', $name);
$response = $email->send('Welcome to modulusPHP.', 'Welcome');
```

You can easily check if an email was successfully sent or not.

This is how you can check if an email was successfully sent:
```php
if ($response['status'] == 'success') {
  // the email was successfully sent
}
```

And this is how you can check if an email was not successfully sent:
```php
if ($response['status'] == 'failed') {
  // the email was successfully sent
}
```

If an email fails to send, you will be able to check for the `reason` like:
```php
$response['reason'];
```

Example:
```php
if ($response['status'] == 'failed') {
  Log::info('Could send email, ' . $response['reason']);
}
```

## to
Primary recipients.

Example:
```php
$email->to('donald@foo', 'Donald Pakkies');
```

### Parameters

Argument  |  Type  |  Description
--------  |  ----  |  -----------
email      |  String  |  The recipients email address.
name   |  String  |  The name of the recipient. (optional)

## from
The sender. (optional)

Example:
```php
$email->from('no-reply@foo', 'Company Name');
```

### Parameters

Argument  |  Type  |  Description
--------  |  ----  |  -----------
email      |  String  |  The sender's email address.
name   |  String  |  The name of the sender. (optional)

## replyTo
The sender. (optional)

Example:
```php
$email->replyTo('info@foo', 'Company Name');
```

### Parameters

Argument  |  Type  |  Description
--------  |  ----  |  -----------
email      |  String  |  The sender's email address.
name   |  String  |  The name of the sender. (optional)

## bcc
Blind carbon copy to tertiary recipients who receive the message. The primary and secondary recipients cannot see the tertiary recipients. Depending on email software, the tertiary recipients may only see their own email address in Bcc, or they may see the email addresses of all primary and secondary recipients. (optional)

Example:
```php
$email->bcc('donald@foo', 'Donald');
$email->bcc('loki@foo');
```

### Parameters

Argument  |  Type  |  Description
--------  |  ----  |  -----------
email      |  String  |  The recipients email address.
name   |  String  |  The name of the recipient. (optional)

## cc
Carbon copy to secondary recipients—other interested parties. (optional)

Example:
```php
$email->cc('donald@foo', 'Donald');
$email->cc('loki@foo');
```

### Parameters

Argument  |  Type  |  Description
--------  |  ----  |  -----------
email      |  String  |  The recipients email address.
name   |  String  |  The name of the recipient. (optional)

## attachment
File sent along with an email message. One or more files can be attached to any email message, and be sent along with it to the recipient. (optional)

Example:
```php
$email->attachment('/var/tmp/file.tar.gz');
$email->attachment('/tmp/image.jpg', 'new.jpg');
```

### Parameters

Argument  |  Type  |  Description
--------  |  ----  |  -----------
file      |  File  |  File sent along with an email message.
name   |  String  |  The name of the file. (optional)

## view
With the `view` method, we can easily attach an existsing Modulus Touch view to an email. Here are a few examples:

```php
$email = new Mail;
$name = 'Donald Pakkies';

$email->to('donald@foo', $name);
$email->view('app.email.welcome', compact('name'));
$response = $email->send('welcome to modulusPHP.', 'Welcome');
```

The view:
```html
<!-- View stored in resources/views/app/email/welcome.modulus.php -->

<html>
    <body>
        <h1>Hey {{ $name }}</h1>
        <p>{{ $body }}</p>
    </body>
</html>
```

Output:

> <h1>Hey Donald Pakkies</h1>

> Welcome to modulusPHP.

See [views](1.3/basics/views.md) and [Modulus Touch](1.3/frontend/modulus.md) for more information on how to get started with `views`.