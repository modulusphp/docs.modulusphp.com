# API's

<a name="quickstart"></a>
## Quick Start

modulusPHP makes it easy to quickly get started with building Restful api's.

Let's look at a quick example of a simple API response:

```php
<?php

namespace App\Http\Controllers;

use App\Core\Log;
use ModulusPHP\Http\Rest;
use ModulusPHP\Http\Status;
use App\Http\Controllers\Controller;
use ModulusPHP\Http\Requests\Request;

class ApiController extends Controller
{
  public function getUser(Request $request)
  {
    if ($request->hasInput('id')) {
      $user_id = $request->input('id');

      $user = User::where('hashid', $user_id)->first();
      return Rest::response([
        'status' => 'success',
        'user' => $user
      ], 200);
    }

    return Rest::response([
      'status' => 'failed',
      'reason' => 'id not specified'
    ], 422);
  }

}
```

We first check if the `request` has an `id`, then we look for a user with that specific id. This is not the ideal way of building Restful api's but it does get the job done.

### Cleaning up API's

Its good practice to let middleware handle the heavy work and let the controllers just return data.

Let's look at how we can go about doing this:

Add a new middleware.

```
php modulus make:middleware GetUser
```

Then register the middleware in the `HttpFoundation`:

```php
// Within App\Http\HttpFoundation Class...

protected $middleware = [
  ...,
  'getuser' => \App\Http\Middleware\GetUser::class,
];
```

Add the middleware in the routes:

```
Route::group(['middleware' => 'getuser'], function() {
  Route::post('/api/users/get', 'ApiController@getUser');
});
```

The middleware:

```php
namespace App\Http\Middleware;

use App\Core\Auth;
use ModulusPHP\Http\Rest;
use ModulusPHP\Http\Requests\Request;

class GetUser
{
  /**
   * Handle an incoming request.
   */
  public function handle(Request $request)
  {
    if ($request->hasInput('id')) {

      if (User::where('hashid', $request->input('id'))->first() != null) {
        return true;
      }

      return Rest::response([
        'status' => 'failed',
        'reason' => 'user does not exist.'
      ], 404);
    }

    return Rest::response([
      'status' => 'failed',
      'reason' => 'id not specified'
    ], 422);

  }
```

Now your controller should look like:

```php
<?php

namespace App\Http\Controllers;

use App\Core\Log;
use ModulusPHP\Http\Rest;
use ModulusPHP\Http\Status;
use App\Http\Controllers\Controller;
use ModulusPHP\Http\Requests\Request;

class ApiController extends Controller
{
  public function getUser(Request $request)
  {
    $user = User::where('hashid', $user_id)->first();
    return Rest::response([
      'status' => 'success',
      'user' => $user
    ], 200);
  }

}
```