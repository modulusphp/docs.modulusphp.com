# File

<a name="introduction"></a>
## Introduction

The Modulus File component allows you to easily handle file uploads. Here's how you can get started with the File component.

```php
<?php

namespace App\Http\Controllers;

use App\Models\User;
use ModulusPHP\File\File;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
  public function upload(Request $request)
  {
    if ($request->hasFile('image')) {
      $name = md5(mt_rand()); // example: "14ac6210ec07813cbc2533f09e0c0ed6"
      $image = $request->file('image');

      $response = (new File)->upload($image, false, $name, true);
      Log::info($response);
    }
  }
}
```

First, we generate a file `$name` then we store the image in a variable, then we upload the image, then we finally output the response in the log file.

The response will look something like this:
```php
Array
(
    [status] => success
    [path] => ../public/uploads/14ac6210ec07813cbc2533f09e0c0ed6.png
)
```

## upload
Upload is used when you want to store a file on a server. You can pass all the expected arguments if you want to make changes to the file. To know if a upload was successful or not, you can capture the response of the upload method.

### Parameters

Argument  |  Type  |  Description
--------  |  ----  |  -----------
file      |  File  |  The `file` you want to upload.
privacy   |  Boolean  |  If `true` the file will be saved under the storage directory, if false, it will be stored under the public directory. (default: `true`)
name      |  String  |  The `name` of the file (if not specified, the original file `name` will be used).
Extension  |  Boolean  | If you change the file `name`, you can specify if you would still like to use the default file extension. (default `true`).

Basic usage:
```php
$image = $request->file('image');

$file = new File;
$response = $file->file($image);
```

You can easily check if a file was successfully uploaded or not.

To check if a upload was successful, you can do the following:
```php
if ($response['status'] == 'success') {
  // file was successfully uploaded
}
```

To check if a file upload was not successful, you can do the following:
```php
if ($response['status'] == 'failed') {
  // file was not successfully uploaded
}
```

If a file upload was successful, the response will return a `path` of the file.
Example:
```php
$response['path'];
```

If a file upload was not successful, the response will return a `reason`.
Example:
```php
$response['reason'];
```

## location
The File class allows you to change set the location of the file uploads. You can do this when creating a new File instance or you can use the `location` method.

Here's an example:
```php
$file = new File('images/profilepics');
```

Or:
```php
$file = new File;
$file->location('images/profilepics');
```