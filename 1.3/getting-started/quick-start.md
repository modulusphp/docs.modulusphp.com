## Configuration

Before serving your application, it is recommended to set your application details in the `.env` file. The `.env` file should be saved in the root directory. See `.env.example` for more details.

```env
# Application
APP_NAME=modulusPHP
APP_ENV=local
APP_DEBUG=true
APP_URL=http://localhost
APP_ROOT=/public

# Database
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=modulusphp
DB_USERNAME=root
DB_PASSWORD=secret

# Mail
MAIL_USERNAME=example@domain.com
MAIL_PASSWORD=secret
MAIL_FROM_NAME=foo
MAIL_HOST=smtp.domain.com
MAIL_PORT=465 # or 587
MAIL_SMTP_SECURE=ssl # or tls

# C Modulus
C_MODULUS_ENABLE=false
```

To start the application, you first need to make sure that all migrations have been ran. To run the migrations, simply run the following command in the terminal.

```bash
php modulus migrate
```

If the database you specified in the `.env` file does not exist, the migration will fail.

## Directory Structure

The default modulusPHP application structure is intended to provide a great starting point for both large and small applications.

```
root
    app
        Config
        Http
            Controllers
        Models
    public
    resourses
        views
    storage
        logs
        tokens
        uploads
    vendor
```

#### The App Directory

The `app` directory, contains all of your application's configuration files, controllers and models.

#### The Public Directory

The `public` directory contains the `index.php` file, which is the entry point for all requests entering your application and configures autoloading. This directory also houses your assets such as images, JavaScript, and CSS.

#### The Resources Directory

The `resources` directory contains your views.

#### The Storage Directory

The `storage` directory contains your logs, database migrations, tokens and uploads.

#### The Vendor Directory

The `vendor` directory contains your [Composer](https://getcomposer.org) dependencies.

## Preview your site

Run the local server with `php modulus serve`. You can preview your site in your browser on `http://localhost:8000`.

```bash
php modulus serve
```
