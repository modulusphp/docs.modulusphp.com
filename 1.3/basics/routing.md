# Routing

<a name="basic-routing"></a>
## Basic Routing

The most basic modulusPHP routes accept a URI and a `Closure`, providing a very simple and expressive method of defining routes:

```php
Route::get('/foo', function () {
  return 'Hello World';
});
```

#### The Default Route Files

All modulusPHP routes are defined in your route files, which are located in the `routes` directory. These files are automatically loaded by the `application`. The `routes/web.php` file defines routes that are for your web interface.

For most applications, you will begin by defining routes in your `routes/web.php` file. The routes defined in `routes/web.php` may be accessed by entering the defined route's URL in your browser. For example, you may access the following route by navigating to `example.com/user` in your browser:

```php
Route::get('/user', 'UserController@index');
```

#### Available Router Methods

The router allows you to register routes that respond to any HTTP verb:

```php
Route::get($uri, $callback);
Route::post($uri, $callback);
Route::put($uri, $callback);
Route::patch($uri, $callback);
Route::delete($uri, $callback);
Route::vue($callback);
```

<a name="route-parameters"></a>
## Route Parameters

<a name="required-parameters"></a>
### Required Parameters

Of course, sometimes you will need to capture segments of the URI within your route. For example, you may need to capture a user's ID from the URL. You may do so by defining route parameters:

```php
Route::get('/user/{id}', function ($args) {
  return 'User '.$args->id;
});
```

Route parameters are always encased within `{}` braces and should consist of alphabetic characters, and may not contain a `-` character. Instead of using the `-` character, use an underscore (`_`).

## Vue Routing

modulusPHP has a router method for vuejs. This router method is responsible for loading a `view` that contains the `<router-view></router-view>` tag.

```php
// code in routes/web.php

Route::vue(function() {
    view('welcome');
});
```

```html
{{-- code in resources/views/welcome.modulus.php --}}

{% partials('layouts.default') %}

{% in('title') %}
    Home | modulusPHP
{% endin %}

{% in('main') %}

    <router-view></router-view>

{% endin %}
```

```js
// code stored in resources/assets/js/routes.js

export default [
    {
        path: '/',
        name: 'Home',
        component: require('./components/app/home.vue')
    },
    {
        path: '*',
        name: 'notfound',
        component: require('./components/app/404.vue')
    }
];
```

> for more information, visit: https://router.vuejs.org/api/