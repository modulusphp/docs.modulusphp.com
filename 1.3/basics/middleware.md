# Middleware

## Introduction

Middleware provide a convenient mechanism for filtering HTTP requests entering your application. For example, modulusPHP includes a middleware that verifies the user of your application is authenticated. If the user is not authenticated, the middleware will redirect the user to the login screen. However, if the user is authenticated, the middleware will allow the request to proceed further into the application.

Of course, additional middleware can be written to perform a variety of tasks besides authentication.

There are a few middleware included in the modulusPHP framework, including middleware for `authentication`. All of these middleware are located in the `app/Http/Middleware` directory.

<a name="defining-middleware"></a>
## Defining Middleware

To create a new middleware, use the `make:middleware` Modulus command:

    php modulus make:middleware CheckAge

This command will place a new `Checkage` class within your `app/Http/Middleware` directory. In this middleware, we will only allow access to the route if the supplied `age` is greater than 200. Otherwise, we will redirect the users back to the `home` URI.

```php
<?php

namespace App\Http\Middleware;

use App\Core\Auth;
use ModulusPHP\Http\Requests\Request;

class CheckAge
{
    /**
      * Handle an incoming request.
      */
    public function handle(Request $request)
    {
        if (Auth::user()->age <= 200) {
            return redirect();
        }

        return true;
    }
}
```

As you can see, if the given `age` is less than or equal to `200`, the middleware will return an HTTP redirect to the client; otherwise, the request will be passed further into the application.

If a `middleware` is successful, you must return `true`, and if it fails, you must return `false`.


It's best to envision middleware as a series of "layers" HTTP requests must pass through before they hit your application. Each layer can examine the request and even reject it entirely.

<a name="registering-middleware"></a>
## Registering Middleware

<a name="global-middleware"></a>
### Global Middleware

If you want a middleware to run during every HTTP request to your application, list the middleware class in the `$middleware` property of your `app/Http/HttpFoundation.php` class.

<a name="assigning-middleware-to-routes"></a>
### Assigning Middleware To Routes

If you would like to assign middleware to specific routes, you should first assign the middleware a key in your `app/Http/HttpFoundation.php` file. By default, the `$middleware` property of this class contains entries for the middleware included with modulusPHP. To add your own, append it to this list and assign it a key of your choosing. For example:

```php
// Within App\Http\HttpFoundation Class...

protected $middleware = [
    'auth' => \ModulusPHP\Http\Middleware\Authenticate::class,
    'guest' => \ModulusPHP\Http\Middleware\GuestMiddleware::class,
    'dev' => \App\Http\Middleware\DevelopmentMiddleware::class
];
```

Once the middleware has been defined in the HTTP kernel, you may use the `middleware` method to assign middleware to a route:

```php
Route::group(['middleware' => 'auth'], function() {

  Route::get('/admin/profile', 'AdminController@profile');

});
```

You may also assign multiple middleware to the route:

```php
Route::group(['middleware' => ['first', 'second']], function() {

  Route::get('/admin/profile', 'AdminController@profile');

});
```