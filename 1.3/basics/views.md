# Views

## Creating Views

Views contain the HTML served by your application and separate your controller / application logic from your presentation logic. Views are stored in the `resources/views` directory. A simple view might look something like this:

```html
<!-- View stored in resources/views/greeting.modulus.php -->

<html>
    <body>
        <h1>Hello, {{ $name }}</h1>
    </body>
</html>
```

## Passing Data To Views

We can pass data from a Controller, to a view like:

```php
class GreetController extends Controller
{
    /**
      * Greeting Controller
      *
      * @return void
      */
    public function index($name)
    {
        return view('greeting', ['name' => $name]);
    }
}
```

Route:

    Route::get('/greet/{name}', 'GreetController@index');

Example:

    example.com/greet/donald

Results:

**Hello, donald**

You can pass multiple variables:

```php
class GreetController extends Controller
{
    /**
      * Greeting Controller
      *
      * @return void
      */
    public function index($name, $age)
    {
        return view('greeting', compact('name', 'age'));
    }
}
```

View:

```html
<!-- View stored in resources/views/greeting.modulus.php -->

<html>
    <body>
        <h1>Hello, {{ $name }}. You are {{ $age }} years old</h1>
    </body>
</html>
```

Route:

    Route::get('/greet/{name}/{age}', 'GreetController@index');

Example:

    example.com/greet/donald/20

Results:

**Hello, donald. You are 20 years old**

As you can see, you can pass more than 1 variable.