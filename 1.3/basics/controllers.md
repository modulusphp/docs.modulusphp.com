# Controllers

## Introduction

modulusPHP Controllers are used to handle logic. Controllers are stored in the `app/Controllers` directory.

## Basic Controllers

### Defining Controllers

Below is an example of a basic controller class. Note that the controller extends the base controller class included with modulusPHP. The base class provides a few convenience methods such as the `middleware` method:

```php
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
      * Show the profile for the given user.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }
}
```

Route for this method:

```php
Route::get('/profile/{id}, 'UserController@show');
```

This method can be accessed like:

    example.com/profile/{id}

The request will look for the UserController and execute show method. The specifed paramters will also be passed, in this case, the id parameter will be passed to the index method.

## Handling Post Requests

`Post` requests are treated the same as `Get` requests. Post data is sent as an Array:

```php
use App\Core\Auth;
use App\Models\User;
use ModulusPHP\Http\Requests\Request;

class UserController extends Controller
{
    /**
      * Handle a post/get request
      *
      * @return void
      */
    public function update(Request $request)
    {
        $user = User:find(Auth::user()->id)->first();

        if ($request->method() == Request::POST)
        {
            $user->username = $request->input('username');
            $user->save()

            return view('user.update', ['user' => $user, 'message' => 'Your username was successfully changed']);
        }

        return view('user.update', compact('user'));
    }
}
```

This method can also be accessed as a `Get`.

Routes:

```php
Route::get('/profile/update', 'UserController@update');
Route::post('/profile/update', 'UserController@update');
```

## Make Controller

You can create a new controller using the modulusPHP cli tool.

```bash
php modulus make:controller UserController
```

A `UserController` will be created.
