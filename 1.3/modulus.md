# Modulus CLI

<a name="introduction"></a>
## Introduction

Modulus is the command-line interface included with modulusPHP. It provides a number of helpful commands that can assist you while you build your application. To view a list of all available Modulus commands, you may use the list command:

```
php modulus list
```

Every command also includes a "help" screen which displays and describes the command's available arguments and options. To view a help screen, precede the name of the command with help:

```
php modulus help
```