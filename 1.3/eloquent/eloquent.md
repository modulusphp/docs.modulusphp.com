# Eloquent: Getting Started

<a name="introduction"></a>
## Introduction

The Eloquent ORM included with modulusPHP provides a beautiful, simple ActiveRecord implementation for working with your database. Each database table has a corresponding "Model" which is used to interact with that table. Models allow you to query for data in your tables, as well as insert new records into the table.

Before getting started, be sure to configure a database connection in `config/database.php` or `.env`. For more information on configuring your database, check out [the documentation](1.3/getting-started/quick-start?id=configuration).

<a name="defining-models"></a>
## Defining Models

To get started, let's create an Eloquent model. Models typically live in the `app/Models` directory, but you are free to place them anywhere that can be auto-loaded according to your `composer.json` file. All Eloquent models extend `ModulusPHP\Framework\Model` class.

The easiest way to create a model instance is using the `make:model` [Modulus command](/docs/1.3/modulus):

```bash
php modulus make:model Flight
```

<a name="eloquent-model-conventions"></a>
### Eloquent Model Conventions

Now, let's look at an example `Flight` model, which we will use to retrieve and store information from our `flights` database table:

```php
<?php

use ModulusPHP\Framework\Model;

class Flight extends Model
{
  //
}
```

#### Table Names

Note that we did not tell Eloquent which table to use for our `Flight` model. By convention, the "snake case", plural name of the class will be used as the table name unless another name is explicitly specified. So, in this case, Eloquent will assume the `Flight` model stores records in the `flights` table. You may specify a custom table by defining a `table` property on your model:

```php
<?php

use ModulusPHP\Framework\Model;

class Flight extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'my_flights';
}
```

#### Primary Keys

Eloquent will also assume that each table has a primary key column named `id`. You may define a protected `$primaryKey` property to override this convention.

In addition, Eloquent assumes that the primary key is an incrementing integer value, which means that by default the primary key will be cast to an `int` automatically. If you wish to use a non-incrementing or a non-numeric primary key you must set the public `$incrementing` property on your model to `false`. If your primary key is not an integer, you should set the protected `$keyType` property on your model to `string`.

#### Timestamps

By default, Eloquent expects `created_at` and `updated_at` columns to exist on your tables.  If you do not wish to have these columns automatically managed by Eloquent, set the `$timestamps` property on your model to `false`:

```php
<?php

use ModulusPHP\Framework\Model;

class Flight extends Model
{
  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = false;
}
```

If you need to customize the format of your timestamps, set the `$dateFormat` property on your model. This property determines how date attributes are stored in the database, as well as their format when the model is serialized to an array or JSON:

```php
<?php

use ModulusPHP\Framework\Model;

class Flight extends Model
{
  /**
   * The storage format of the model's date columns.
   *
   * @var string
   */
  protected $dateFormat = 'U';
}
```

If you need to customize the names of the columns used to store the timestamps, you may set the `CREATED_AT` and `UPDATED_AT` constants in your model:

```php
<?php

class Flight extends Model
{
  const CREATED_AT = 'creation_date';
  const UPDATED_AT = 'last_update';
}
```

#### Database Connection

By default, all Eloquent models will use the default database connection configured for your application. If you would like to specify a different connection for the model, use the `$connection` property:

```php
<?php

use ModulusPHP\Framework\Model;

class Flight extends Model
{
  /**
   * The connection name for the model.
   *
   * @var string
   */
  protected $connection = 'connection-name';
}
```

<a name="retrieving-models"></a>
## Retrieving Models

Once you have created a model and [its associated database table](1.3/database/migrations?id=migration-structure), you are ready to start retrieving data from your database. Think of each Eloquent model as a powerful [query builder](1.3/database/queries) allowing you to fluently query the database table associated with the model. For example:

```php
<?php

use Flight;

$flights = Flight::all();

foreach ($flights as $flight) {
  echo $flight->name;
}
```

#### Adding Additional Constraints

The Eloquent `all` method will return all of the results in the model's table. Since each Eloquent model serves as a [query builder](1.3/database/queries), you may also add constraints to queries, and then use the `get` method to retrieve the results:

```php
$flights = Flight::where('active', 1)
                   ->orderBy('name', 'desc')
                   ->take(10)
                   ->get();
```

> {tip} Since Eloquent models are query builders, you should review all of the methods available on the [query builder](1.3/database/queries). You may use any of these methods in your Eloquent queries.

<a name="collections"></a>
### Collections

For Eloquent methods like `all` and `get` which retrieve multiple results, an instance of `Illuminate\Database\Eloquent\Collection` :

```php
$flights = $flights->reject(function ($flight) {
  return $flight->cancelled;
});
```

Of course, you may also loop over the collection like an array:

```php
foreach ($flights as $flight) {
  echo $flight->name;
}
```

<a name="chunking-results"></a>
### Chunking Results

If you need to process thousands of Eloquent records, use the `chunk` command. The `chunk` method will retrieve a "chunk" of Eloquent models, feeding them to a given `Closure` for processing. Using the `chunk` method will conserve memory when working with large result sets:

```php
Flight::chunk(200, function ($flights) {
  foreach ($flights as $flight) {
      //
  }
});
```

The first argument passed to the method is the number of records you wish to receive per "chunk". The Closure passed as the second argument will be called for each chunk that is retrieved from the database. A database query will be executed to retrieve each chunk of records passed to the Closure.

#### Using Cursors

The `cursor` method allows you to iterate through your database records using a cursor, which will only execute a single query. When processing large amounts of data, the `cursor` method may be used to greatly reduce your memory usage:

```php
foreach (Flight::where('foo', 'bar')->cursor() as $flight) {
  //
}
```

<a name="retrieving-single-models"></a>
## Retrieving Single Models / Aggregates

Of course, in addition to retrieving all of the records for a given table, you may also retrieve single records using `find` or `first`. Instead of returning a collection of models, these methods return a single model instance:

```php
// Retrieve a model by its primary key...
$flight = Flight::find(1);

// Retrieve the first model matching the query constraints...
$flight = Flight::where('active', 1)->first();
```

You may also call the `find` method with an array of primary keys, which will return a collection of the matching records:

```php
$flights = Flight::find([1, 2, 3]);
```

#### Not Found Exceptions

Sometimes you may wish to throw an exception if a model is not found. This is particularly useful in routes or controllers. The `findOrFail` and `firstOrFail` methods will retrieve the first result of the query; however, if no result is found, a `Illuminate\Database\Eloquent\ModelNotFoundException` will be thrown:

```php
$model = Flight::findOrFail(1);

$model = Flight::where('legs', '>', 100)->firstOrFail();
```

If the exception is not caught, a `404` HTTP response is automatically sent back to the user. It is not necessary to write explicit checks to return `404` responses when using these methods:

```php
Route::get('/api/flights/{id}', function ($args) {
    return Flight::findOrFail($args->id);
});
```

<a name="retrieving-aggregates"></a>
### Retrieving Aggregates

You may also use the `count`, `sum`, `max`, and other [aggregate methods](1.3/database/queries#aggregates) provided by the [query builder](1.3/database/queries). These methods return the appropriate scalar value instead of a full model instance:

```php
$count = Flight::where('active', 1)->count();

$max = Flight::where('active', 1)->max('price');
```

<a name="inserting-and-updating-models"></a>
## Inserting & Updating Models

<a name="inserts"></a>
### Inserts

To create a new record in the database, create a new model instance, set attributes on the model, then call the `save` method:

```php
<?php

namespace App\Http\Controllers;

use App\Models\Flight;
use ModulusPHP\Http\Requests\Request;

class FlightController extends Controller
{
  /**
   * Create a new flight instance.
   *
   * @param  Request  $request
   * @return Response
   */
  public function store(Request $request)
  {
    // Validate the request...

    $flight = new Flight;

    $flight->name = $request->name;

    $flight->save();
  }
}
```

In this example, we assign the `name` parameter from the incoming HTTP request to the `name` attribute of the `Flight` model instance. When we call the `save` method, a record will be inserted into the database. The `created_at` and `updated_at` timestamps will automatically be set when the `save` method is called, so there is no need to set them manually.

<a name="updates"></a>
### Updates

The `save` method may also be used to update models that already exist in the database. To update a model, you should retrieve it, set any attributes you wish to update, and then call the `save` method. Again, the `updated_at` timestamp will automatically be updated, so there is no need to manually set its value:

```php
$flight = Flight::find(1);

$flight->name = 'New Flight Name';

$flight->save();
```

#### Mass Updates

Updates can also be performed against any number of models that match a given query. In this example, all flights that are `active` and have a `destination` of `San Diego` will be marked as delayed:

```php
Flight::where('active', 1)
          ->where('destination', 'San Diego')
          ->update(['delayed' => 1]);
```

The `update` method expects an array of column and value pairs representing the columns that should be updated.

> {note} When issuing a mass update via Eloquent, the `saved` and `updated` model events will not be fired for the updated models. This is because the models are never actually retrieved when issuing a mass update.

<a name="mass-assignment"></a>
### Mass Assignment

You may also use the `create` method to save a new model in a single line. The inserted model instance will be returned to you from the method. However, before doing so, you will need to specify either a `fillable` or `guarded` attribute on the model, as all Eloquent models protect against mass-assignment by default.

A mass-assignment vulnerability occurs when a user passes an unexpected HTTP parameter through a request, and that parameter changes a column in your database you did not expect. For example, a malicious user might send an `is_admin` parameter through an HTTP request, which is then passed into your model's `create` method, allowing the user to escalate themselves to an administrator.

So, to get started, you should define which model attributes you want to make mass assignable. You may do this using the `$fillable` property on the model. For example, let's make the `name` attribute of our `Flight` model mass assignable:

```php
<?php

use ModulusPHP\Framework\Model;

class Flight extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name'];
}
```

Once we have made the attributes mass assignable, we can use the `create` method to insert a new record in the database. The `create` method returns the saved model instance:

```php
$flight = Flight::create(['name' => 'Flight 10']);
```

If you already have a model instance, you may use the `fill` method to populate it with an array of attributes:

```php
$flight->fill(['name' => 'Flight 22']);
```

#### Guarding Attributes

While `$fillable` serves as a "white list" of attributes that should be mass assignable, you may also choose to use `$guarded`. The `$guarded` property should contain an array of attributes that you do not want to be mass assignable. All other attributes not in the array will be mass assignable. So, `$guarded` functions like a "black list". Of course, you should use either `$fillable` or `$guarded` - not both. In the example below, all attributes **except for `price`** will be mass assignable:

```php
<?php

    use ModulusPHP\Framework\Model;

    class Flight extends Model
    {
        /**
         * The attributes that aren't mass assignable.
         *
         * @var array
         */
        protected $guarded = ['price'];
    }
```

If you would like to make all attributes mass assignable, you may define the `$guarded` property as an empty array:

```php
/**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
protected $guarded = [];
```

<a name="other-creation-methods"></a>
### Other Creation Methods

#### `firstOrCreate`/ `firstOrNew`

There are two other methods you may use to create models by mass assigning attributes: `firstOrCreate` and `firstOrNew`. The `firstOrCreate` method will attempt to locate a database record using the given column / value pairs. If the model can not be found in the database, a record will be inserted with the attributes from the first parameter, along with those in the optional second parameter.

The `firstOrNew` method, like `firstOrCreate` will attempt to locate a record in the database matching the given attributes. However, if a model is not found, a new model instance will be returned. Note that the model returned by `firstOrNew` has not yet been persisted to the database. You will need to call `save` manually to persist it:

```php
// Retrieve flight by name, or create it if it doesn't exist...
$flight = Flight::firstOrCreate(['name' => 'Flight 10']);

// Retrieve flight by name, or create it with the name and delayed attributes...
$flight = Flight::firstOrCreate(
    ['name' => 'Flight 10'], ['delayed' => 1]
);

// Retrieve by name, or instantiate...
$flight = Flight::firstOrNew(['name' => 'Flight 10']);

// Retrieve by name, or instantiate with the name and delayed attributes...
$flight = Flight::firstOrNew(
    ['name' => 'Flight 10'], ['delayed' => 1]
);
```

#### `updateOrCreate`

You may also come across situations where you want to update an existing model or create a new model if none exists. Eloquent provides an `updateOrCreate` method to do this in one step. Like the `firstOrCreate` method, `updateOrCreate` persists the model, so there's no need to call `save()`:

```php
// If there's a flight from Oakland to San Diego, set the price to $99.
// If no matching model exists, create one.
$flight = Flight::updateOrCreate(
    ['departure' => 'Oakland', 'destination' => 'San Diego'],
    ['price' => 99]
);
```

<a name="deleting-models"></a>
## Deleting Models

To delete a model, call the `delete` method on a model instance:

```php
$flight = Flight::find(1);

$flight->delete();
```

#### Deleting An Existing Model By Key

In the example above, we are retrieving the model from the database before calling the `delete` method. However, if you know the primary key of the model, you may delete the model without retrieving it. To do so, call the `destroy` method:

```php
Flight::destroy(1);

Flight::destroy([1, 2, 3]);

Flight::destroy(1, 2, 3);
```

#### Deleting Models By Query

Of course, you may also run a delete statement on a set of models. In this example, we will delete all flights that are marked as inactive. Like mass updates, mass deletes will not fire any model events for the models that are deleted:

```php
$deletedRows = Flight::where('active', 0)->delete();
```

> {note} When executing a mass delete statement via Eloquent, the `deleting` and `deleted` model events will not be fired for the deleted models. This is because the models are never actually retrieved when executing the delete statement.
