# Eloquent: Collections

<a name="introduction"></a>
## Introduction

All multi-result sets returned by Eloquent are instances of the `Illuminate\Database\Eloquent\Collection` object, including results retrieved via the `get` method or accessed via a relationship. The Eloquent collection object extends the [base collection](/1.3/eloquent/collections), so it naturally inherits dozens of methods used to fluently work with the underlying array of Eloquent models.

Of course, all collections also serve as iterators, allowing you to loop over them as if they were simple PHP arrays:

    $users = App\User::where('active', 1)->get();

    foreach ($users as $user) {
        echo $user->name;
    }

However, collections are much more powerful than arrays and expose a variety of map / reduce operations that may be chained using an intuitive interface. For example, let's remove all inactive models and gather the first name for each remaining user:

    $users = App\User::all();

    $names = $users->reject(function ($user) {
        return $user->active === false;
    })
    ->map(function ($user) {
        return $user->name;
    });

> {note} While most Eloquent collection methods return a new instance of an Eloquent collection, the `pluck`, `keys`, `zip`, `collapse`, `flatten` and `flip` methods return a [base collection](/1.3/eloquent/collections) instance. Likewise, if a `map` operation returns a collection that does not contain any Eloquent models, it will be automatically cast to a base collection.

<a name="available-methods"></a>
## Available Methods

### The Base Collection

All Eloquent collections extend the [base collection](/1.3/eloquent/collections) object; therefore, they inherit all of the powerful methods provided by the base collection class:

<style>
    #collection-method-list > p {
        column-count: 3; -moz-column-count: 3; -webkit-column-count: 3;
        column-gap: 2em; -moz-column-gap: 2em; -webkit-column-gap: 2em;
    }

    #collection-method-list a {
        display: block;
    }
</style>

<div id="collection-method-list" markdown="1">

[all](/1.3/eloquent/collections#all)
[average](/1.3/eloquent/collections#average)
[avg](/1.3/eloquent/collections#avg)
[chunk](/1.3/eloquent/collections#chunk)
[collapse](/1.3/eloquent/collections#collapse)
[combine](/1.3/eloquent/collections#combine)
[concat](/1.3/eloquent/collections#concat)
[contains](/1.3/eloquent/collections#contains)
[containsStrict](/1.3/eloquent/collections#containsstrict)
[count](/1.3/eloquent/collections#count)
[crossJoin](/1.3/eloquent/collections#crossjoin)
[dd](/1.3/eloquent/collections#dd)
[diff](/1.3/eloquent/collections#diff)
[diffKeys](/1.3/eloquent/collections#diffkeys)
[dump](/1.3/eloquent/collections#dump)
[each](/1.3/eloquent/collections#each)
[eachSpread](/1.3/eloquent/collections#eachspread)
[every](/1.3/eloquent/collections#every)
[except](/1.3/eloquent/collections#except)
[filter](/1.3/eloquent/collections#filter)
[first](/1.3/eloquent/collections#first)
[flatMap](/1.3/eloquent/collections#flatmap)
[flatten](/1.3/eloquent/collections#flatten)
[flip](/1.3/eloquent/collections#flip)
[forget](/1.3/eloquent/collections#forget)
[forPage](/1.3/eloquent/collections#forpage)
[get](/1.3/eloquent/collections#get)
[groupBy](/1.3/eloquent/collections#groupby)
[has](/1.3/eloquent/collections#has)
[implode](/1.3/eloquent/collections#implode)
[intersect](/1.3/eloquent/collections#intersect)
[isEmpty](/1.3/eloquent/collections#isempty)
[isNotEmpty](/1.3/eloquent/collections#isnotempty)
[keyBy](/1.3/eloquent/collections#keyby)
[keys](/1.3/eloquent/collections#keys)
[last](/1.3/eloquent/collections#last)
[map](/1.3/eloquent/collections#map)
[mapInto](/1.3/eloquent/collections#mapinto)
[mapSpread](/1.3/eloquent/collections#mapspread)
[mapToGroups](/1.3/eloquent/collections#maptogroups)
[mapWithKeys](/1.3/eloquent/collections#mapwithkeys)
[max](/1.3/eloquent/collections#max)
[median](/1.3/eloquent/collections#median)
[merge](/1.3/eloquent/collections#merge)
[min](/1.3/eloquent/collections#min)
[mode](/1.3/eloquent/collections#mode)
[nth](/1.3/eloquent/collections#nth)
[only](/1.3/eloquent/collections#only)
[pad](/1.3/eloquent/collections#pad)
[partition](/1.3/eloquent/collections#partition)
[pipe](/1.3/eloquent/collections#pipe)
[pluck](/1.3/eloquent/collections#pluck)
[pop](/1.3/eloquent/collections#pop)
[prepend](/1.3/eloquent/collections#prepend)
[pull](/1.3/eloquent/collections#pull)
[push](/1.3/eloquent/collections#push)
[put](/1.3/eloquent/collections#put)
[random](/1.3/eloquent/collections#random)
[reduce](/1.3/eloquent/collections#reduce)
[reject](/1.3/eloquent/collections#reject)
[reverse](/1.3/eloquent/collections#reverse)
[search](/1.3/eloquent/collections#search)
[shift](/1.3/eloquent/collections#shift)
[shuffle](/1.3/eloquent/collections#shuffle)
[slice](/1.3/eloquent/collections#slice)
[sort](/1.3/eloquent/collections#sort)
[sortBy](/1.3/eloquent/collections#sortby)
[sortByDesc](/1.3/eloquent/collections#sortbydesc)
[splice](/1.3/eloquent/collections#splice)
[split](/1.3/eloquent/collections#split)
[sum](/1.3/eloquent/collections#sum)
[take](/1.3/eloquent/collections#take)
[tap](/1.3/eloquent/collections#tap)
[toArray](/1.3/eloquent/collections#toarray)
[toJson](/1.3/eloquent/collections#tojson)
[transform](/1.3/eloquent/collections#transform)
[union](/1.3/eloquent/collections#union)
[unique](/1.3/eloquent/collections#unique)
[uniqueStrict](/1.3/eloquent/collections#uniquestrict)
[unless](/1.3/eloquent/collections#unless)
[values](/1.3/eloquent/collections#values)
[when](/1.3/eloquent/collections#when)
[where](/1.3/eloquent/collections#where)
[whereStrict](/1.3/eloquent/collections#wherestrict)
[whereIn](/1.3/eloquent/collections#wherein)
[whereInStrict](/1.3/eloquent/collections#whereinstrict)
[whereNotIn](/1.3/eloquent/collections#wherenotin)
[whereNotInStrict](/1.3/eloquent/collections#wherenotinstrict)
[zip](/1.3/eloquent/collections#zip)

</div>

<a name="custom-collections"></a>
## Custom Collections

If you need to use a custom `Collection` object with your own extension methods, you may override the `newCollection` method on your model:

    <?php

    namespace App\Models;

    use App\CustomCollection;
    use ModulusPHP\Framework\Model;

    class User extends Model
    {
        /**
         * Create a new Eloquent Collection instance.
         *
         * @param  array  $models
         * @return \Illuminate\Database\Eloquent\Collection
         */
        public function newCollection(array $models = [])
        {
            return new CustomCollection($models);
        }
    }

Once you have defined a `newCollection` method, you will receive an instance of your custom collection anytime Eloquent returns a `Collection` instance of that model. If you would like to use a custom collection for every model in your application, you should override the `newCollection` method on a base model class that is extended by all of your models.
