# Modulus Templates

<a name="introduction"></a>
## Introduction

Modulus Touch is a simple and easy to use templating engine provided with modulusPHP. Modulus Touch view files use the `.modulus.php` file extension and are typically stored in the `resources/views` directory.

<a name="template-inheritance"></a>
## Template Inheritance

<a name="defining-a-layout"></a>
### Defining A Layout

To get started, let's take a look at a simple example. First, we will examine a "master" page layout. Since most web applications maintain the same general layout across various pages, it's convenient to define this layout as a single Modulus Touch view:

    <!-- Stored in resources/views/layouts/default.modulus.php -->

    <html>
        <head>
            <title>App Name - {% tag('title') %}</title>
        </head>
        <body>
            <div class="container">
                {% tag('content') %}
            </div>
        </body>
    </html>

As you can see, this file contains typical HTML mark-up. However, take note of `{% tag('name') %}`. {% tag('name') %}, defines a section of content.

Now that we have defined a layout for our application, let's define a child page that inherits the layout.

<a name="extending-a-layout"></a>
### Extending A Layout

When defining a child view, use `{% partials('name') %}` to specify which layout the child view should "inherit". Views which extend a Modulus Touch layout may inject content into the layout's sections using `{% in('section') %}`. Remember, as seen in the example above, the contents of these sections will be displayed in the layout using `{% tag('name') %}`:

    <!-- Stored in resources/views/child.modulus.php -->

    {% partials('layouts.default') %}

    {% in('title') %}
        Page Title
    {% endin %}

    {% in('content') %}
        <p>This is my body content.</p>
    {% endin %}

Modulus Touch views may be returned from routes using the global `view` helper:

    Route::get('/', function () {
        return view('child');
    });

<a name="displaying-data"></a>
## Displaying Data

You may display data passed to your Modulus Touch views by wrapping the variable in curly braces. For example, given the following route:

    Route::get('greeting', function () {
        return view('welcome', ['name' => 'Samantha']);
    });

You may display the contents of the `name` variable like so:

    Hello, {{ $name }}.

Of course, you are not limited to displaying the contents of the variables passed to the view. You may also echo the results of any PHP function. In fact, you can put any PHP code you wish inside of a Modulus Touch echo statement:

    The current UNIX timestamp is {{ time() }}.

> {tip} Modulus Touch `{{ }}` statements are automatically sent through PHP's `htmlspecialchars` function to prevent XSS attacks.

#### Displaying Unescaped Data

By default, Modulus Touch `{{ }}` statements are automatically sent through PHP's `htmlspecialchars` function to prevent XSS attacks. If you do not want your data to be escaped, you may use the following syntax:

    Hello, {{!! $name !!}}.

> {note} Be very careful when echoing content that is supplied by users of your application. Always use the escaped, double curly brace syntax to prevent XSS attacks when displaying user supplied data.

<a name="control-structures"></a>
## Control Structures

In addition to template inheritance and displaying data, Modulus Touch also provides convenient shortcuts for common PHP control structures, such as conditional statements and loops. These shortcuts provide a very clean, terse way of working with PHP control structures, while also remaining familiar to their PHP counterparts.

<a name="if-statements"></a>
### If Statements

You may construct `if` statements using the `{% if ... %}`, `{% elseif ... %}`, `{% else %}`, and `{% endif %}` directives. These directives function identically to their PHP counterparts:

    {% if count($records) === 1 %}
        I have one record!
    {% elseif count($records) > 1 %}
        I have multiple records!
    {% else %}
        I don't have any records!
    {% endif %}

<a name="loops"></a>
### Loops

In addition to conditional statements, Modulus Touch provides simple directives for working with PHP's loop structures. Again, each of these directives functions identically to their PHP counterparts:

    {% for $i = 0; $i < 10; $i++ %}
        The current value is {{ $i }}
    {% endfor %}

    {% foreach ($users as $user) %}
        <p>This is user {{ $user->id }}</p>
    {% endforeach %}

    {% while true %}
        <p>I'm looping forever.</p>
    {% endwhile %}

<a name="comments"></a>
### Comments

Modulus Touch also allows you to define comments in your views. However, unlike HTML comments, Modulus Touch comments are not included in the HTML returned by your application:

    {{-- This comment will not be present in the rendered HTML --}}

    % // And this comment will not be present in the rendered HTML %

<a name="php"></a>
### PHP

In some situations, it's useful to embed PHP code into your views. You can use `{%  %}` to execute a block of plain PHP within your template:

    {%
        //
    %}

<a name="including-sub-views"></a>
## Including Sub-Views

`{% extend('view') %}` allows you to include a view from within another view.

    <div>
        {% extend('shared.errors') %}

        <form>
            <!-- Form Contents -->
        </form>
    </div>

You can also pass an array of data to the included view:

    {% extend('view.name', ['some' => 'data']) %}

<a name="extending-touch"></a>
## Extending Modulus Touch

Modulus Touch can be easily extended. `Fluent` makes it easy to roll out your own templating engine. A good example is Donald Pakkies' <a href="https://github.com/donaldp/blade">Blade</a> implementation for ModulusPHP.

You can use the Grammar class to extend Modulus Touch. Grammar files are typically stored in the `app/Grammar` directory. Let's take a look a simple:

```php
<?php

namespace App\Grammar;

use ModulusPHP\Touch\Fluent;
use ModulusPHP\Touch\Grammar as Touch;

class Grammar extends Touch implements Fluent
{
  public function handle()
  {
    $code = $this->translate('/\@\bdate\b\((.*)\)/', function($match) {
        return "<?php echo date($match[1]); ?>";
    });

    return $code;
  }
}
```

We need to store our translations inside a variable then return the variable at the end of our translations.

Basic usage:

    <!-- Stored in resources/views/child.modulus.php -->

    {% partials('layouts.default') %}

    {% in('title') %}
        Page Title
    {% endin %}

    {% in('content') %}
        <p>The date is: @date('D M Y').</p>
    {% endin %}

This will display the date.

You can create a new Grammar class using the modulus command.
```bash
php modulus make:grammar ControlStructure
```

You should see something like this.

```php
<?php

namespace App\Grammar;

use ModulusPHP\Touch\Fluent;
use ModulusPHP\Touch\Grammar;

class ControlStructure extends Grammar implements Fluent
{
  public function handle()
  {
    return $this->code;
  }

```

In order to use a Grammar, it needs to be registered and enabled in the `app/Config/grammar.php` config file. Here is an example of the Grammar config file.

```php
<?php

/*
|--------------------------------------------------------------------------
| Application Grammar
|--------------------------------------------------------------------------
|
| This is where you should add new application grammar.
|
*/

return [

  'modulus' => [
    'enabled' => true,
    'class' => ModulusPHP\Touch\Modulus::class
  ],
  'default' => [
    'enabled' => true,
    'class' => App\Grammar\Grammar::class
  ],
  'blade' => [
    'enabled' => false,
    'class' => Don47\Grammar\Blade::class
  ],
  'ControlStructure' => [
    'enabled' => true,
    'class' => App\Grammar\ControlStructure::class
  ],

];
```